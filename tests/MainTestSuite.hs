{-# LANGUAGE FlexibleContexts #-}

module Main (
    main
 ) where

import Test.Framework
import Test.Framework.Providers.QuickCheck2
{-import Test.Framework.Providers.HUnit-}
import Test.QuickCheck
{-import Test.Utils-}

import Data.Monoid

import DiffGeom.Spaces.Vec3Test
import DiffGeom.Spaces.RealTest
import DiffGeom.Spaces.SphereTest
import DiffGeom.Spaces.QuaternionsTest
import DiffGeom.Spaces.RotationsTest

import Test.Generic

main :: IO ()
main = defaultMain $ map (plusTestOptions o) tests
  where
    o = mempty {topt_maximum_generated_tests = Just 1000}

tests :: [Test]
tests =
  [ testGroup "Spaces"
    [ allVec3Tests
    , allDiffRealTests
    , allSphereTests
    , allQuaternTests
    , allRot3Tests
    ]
  , testGroup "Examples"
    [
    ]
  ]
