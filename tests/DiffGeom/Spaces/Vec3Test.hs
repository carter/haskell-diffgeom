module DiffGeom.Spaces.Vec3Test where

import Test.Framework
import Test.Framework.Providers.QuickCheck2
import Test.QuickCheck

import DiffGeom.Classes.VectorSpace
import DiffGeom.Classes.VectorSpaceTest
import DiffGeom.Spaces.Vec3

import Test.Generic

instance Arbitrary Vec3 where
    arbitrary = do
        x <- arbitrarySizedFractional
        y <- arbitrarySizedFractional
        z <- arbitrarySizedFractional
        return $ Vec3 x y z

vec3Tests :: Test
vec3Tests = testGroup "Non-Generic Tests" []

allVec3Tests :: Test
allVec3Tests = testGroup "Vec3" $ vec3Tests : [cgp]
  where
    cgp = testGroup "Classes" $ map mkTest (t :: [GenericTest Vec3])
    t = allRealInnerProductSpaceTests
