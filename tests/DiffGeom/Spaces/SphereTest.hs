module DiffGeom.Spaces.SphereTest where

import Test.Framework
import Test.Framework.Providers.QuickCheck2
import Test.QuickCheck

import DiffGeom.Classes.VectorSpace
import DiffGeom.Classes.Fiber
import DiffGeom.Classes.Smooth
import DiffGeom.Classes.Riem
import DiffGeom.Classes.RiemTest

import DiffGeom.Spaces.Vec3
import DiffGeom.Spaces.Vec3Test
import DiffGeom.Spaces.Sphere

import Test.Generic

instance Arbitrary SpherePt where
    arbitrary = do
        v <-  (arbitrary :: Gen Vec3) `suchThat` (\x -> norm x > 1)
        return $ toSphereUnsafe v

instance Arbitrary SphereTangent where
    arbitrary = do
        (p,v) <- arbitrary
        return $ projSphereTangent p v
    -- only way to shrink is to return the zero at this point
    shrink (SphereTangent (p, v)) = if normSquared v > 0 then [vbzero p] else []

instance Arbitrary SphereCoTangent where
    -- generate a tangent vector then flat it
    arbitrary = fmap flat arbitrary
    -- only way to shrink is to return the zero at this point
    shrink (SphereCoTangent (p, m)) = if normSquared m > 0 then [flat $ vbzero p] else []

-- Specific tests for SpherePts
-- |Tolerance for floating point comparisons in these tests
tol = 1e-9

-- |floating point compare
fpcmp :: Double -> Double -> Bool
fpcmp x y = abs (x - y) <= (abs x + abs y + 1) * tol

propToSphere :: Vec3 -> Property
propToSphere x = (norm x > 0) ==> fpcmp 1 $ normSquared px
  where
    SpherePt px = toSphereUnsafe x

propProjSphereTangent :: (SpherePt,Vec3) -> Property
propProjSphereTangent (pp@(SpherePt p),v) = printTestCase tc $ property $ fpcmp 0 dp
  where
    tc = "dp=" ++ show dp
    dp = dot p vproj
    SphereTangent (_, vproj) = projSphereTangent pp v

propUnitNorm :: SpherePt -> Property
propUnitNorm (SpherePt x) = property $ fpcmp 1 $ normSquared x

sphereTests :: Test
sphereTests = testGroup "Non-Generic Tests"
 [ testProperty     "arbitrary point unit norm" propUnitNorm
 , testProperty     "toSphere unit norm" propToSphere
 , testProperty     "projSphereTangent zero dot prod" propProjSphereTangent
 ]

-- TODO: some unit tests for known exp and logs on the sphere

allSphereTests = testGroup "Sphere" $ sphereTests : [cgp]
  where
    cgp = testGroup "Classes" $ map mkTest (t :: [GenericTest SpherePt])
    t = allRiemTests
