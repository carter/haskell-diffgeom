module DiffGeom.Spaces.RealTest where

import Test.Framework
import Test.Framework.Providers.QuickCheck2
import Test.QuickCheck

import DiffGeom.Classes.TolSpace
import DiffGeom.Classes.VectorSpaceTest
import DiffGeom.Classes.Riem
import DiffGeom.Classes.RiemTest

import DiffGeom.Spaces.Real

import DiffGeom.Algorithms.Frechet

import Test.Generic

-- |First, make DiffReal an instance of Arbitrary, so we can use QuickCheck
instance Arbitrary DiffReal where
    arbitrary = fmap DiffReal arbitrarySizedFractional
    shrink (DiffReal x) = map DiffReal $ shrinkRealFrac x

instance Arbitrary RealBundle where
    arbitrary = do
        p <- arbitrary
        v <- arbitrary
        return $ RealBundle (p, v)
    shrink (RealBundle pv) = map RealBundle $ shrink pv

-- Specific Tests for DiffReals

arithMean:: (Fractional a) => [a] -> a
arithMean xs = sum xs / fromIntegral (length xs)

{- check that reals achieve optimum after their first frechet mean step (with stepsize 1), regardless of starting point
-}
propRealMeanOneStep :: [DiffReal] -> Property
propRealMeanOneStep xs =
    not (null xs) ==> fmean ~~ arithMean xs
  where
    cvgd = frechetMeanFixed xs step 1 (head xs)
    fmean = snd cvgd
    step = 1.0 / fromIntegral (length xs)

realTests :: Test
realTests = testGroup "Non-Generic Tests"
 [ testProperty     "Frechet mean in one step" propRealMeanOneStep
 ]

allDiffRealTests :: Test
allDiffRealTests = testGroup "DiffReal" $ realTests : [cgp]
  where
    cgp = testGroup "Classes" $ map mkTest (t :: [GenericTest DiffReal])
    t = allRealInnerProductSpaceTests ++ allRiemTests
