module DiffGeom.Classes.MetricTest where

import Test.Framework
import Test.Framework.Providers.QuickCheck2
import Test.QuickCheck

import DiffGeom.Classes.TolSpace
import DiffGeom.Classes.TolSpaceTest
import DiffGeom.Classes.Metric

-- |Enable generic testing
import Test.Generic

-- |Tolerance for floating point comparisons in these tests
tol = 1e-7

-- |floating point compare
fpcmp :: Double -> Double -> Bool
fpcmp x y = abs (x - y) < (abs x + abs y + 1) * tol

-- |Test the axioms for a metric space
propDistSquared :: (Metric a) => (a, a) -> Property
propDistSquared (x, y) = property $ distSquared x y `fpcmp` (dist y x ^ 2)

propMetricSym :: (Metric a) => (a, a) -> Property
propMetricSym (x, y) = property $ dist x y `fpcmp` dist y x

propMetricNonNeg :: (Metric a) => (a, a) -> Property
propMetricNonNeg (x, y) = property $ dist x y >= 0

propMetricReflexiveZero :: (Metric a) => a -> Property
propMetricReflexiveZero x = property $ dist x x `fpcmp` 0

propMetricPositive :: (Metric a) => (a, a) -> Property
propMetricPositive (x, y) = not (x ~~ y) ==> dist x y > 0

propTriangleIneq :: (Metric a) => (a, a, a) -> Property
propTriangleIneq (x, y, z) = property $ dist x z <= dist x y + dist y z + tol

-- |Summary of tests
-- TODO: Print tolerance in test names
metricTests :: (Arbitrary a, Show a, Metric a) => [GenericTest a]
metricTests =
 [ genericTestPropertyPair "distSquared = dist^2" propDistSquared
 , genericTestPropertyPair "Distance is symmetric" propMetricSym
 , genericTestPropertyPair "Distance is nonnegative" propMetricNonNeg
 , genericTestProperty "Distance from self is zero" propMetricReflexiveZero
 , genericTestPropertyPair "Distance from other points is positive" propMetricPositive
 , genericTestPropertyTriple "Triangle inequality" propTriangleIneq
 ]

allMetricTests :: (Arbitrary a, Show a, Metric a) => [GenericTest a]
allMetricTests = 
    genericTestGroup "Metric" metricTests : allTolSpaceTests
