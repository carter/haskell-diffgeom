{-# LANGUAGE FlexibleContexts #-}

module DiffGeom.Classes.RiemTest where

import Test.Framework
import Test.Framework.Providers.QuickCheck2
import Test.QuickCheck

import DiffGeom.Classes.TolSpace
import DiffGeom.Classes.Metric
import DiffGeom.Classes.MetricTest
import DiffGeom.Classes.Fiber
import DiffGeom.Classes.Smooth
import DiffGeom.Classes.SmoothTest
import DiffGeom.Classes.Riem

-- |Enable generic testing
import Test.Generic

-- |Should always be able to log(x,x), and result should be zero
propLogZero :: (Riem a) => a -> Property
propLogZero x = property $ l ~~ vbzero x
  where
    l = logMapUnsafe x x

-- dist should be compatible with log map
propDistLog :: (Riem a) => (a, a) -> Property
propDistLog (x, y) = property $ abs (dsq - lsq) < 1e-10
  where
    lsq = riemNormSquared $ logMapUnsafe x y
    dsq = distSquared x y

-- log, then exp. should by identity on y
propExpLogId :: (Riem a) => (a, a) -> Property
propExpLogId (x, y) = property $ el ~~ y
  where
    el = expMap $ logMapUnsafe x y

-- flat and sharp are inverses
propFlatSharp :: (Riem a) => (TangentType a) -> Property
propFlatSharp v = property $ (sharp (flat v)) ~~ v
propSharpFlat :: (Riem a) => (CoTangentType a) -> Property
propSharpFlat m = property $ (flat (sharp m)) ~~ m

-- |Summary of tests
riemTests :: (Arbitrary a,
    Arbitrary (TangentType a),
    Arbitrary (CoTangentType a),
    Show a,
    Show (TangentType a),
    Show (CoTangentType a),
    Riem a) => [GenericTest a]
riemTests =
 [ genericTestProperty "Log of same point is zero" propLogZero
 , genericTestPropertyPair "Distance compatible with log" propDistLog
 , genericTestPropertyPair "Exp . Log is identity" propExpLogId
 , genericTestPropertyTangent "Flat sharp inverse" propFlatSharp
 , genericTestPropertyCoTangent "Sharp flat inverse" propSharpFlat
 ]

allRiemTests :: (Arbitrary a, Show a,
    Arbitrary (TangentType a),
    Arbitrary (CoTangentType a),
    Show (TangentType a),
    Show (CoTangentType a),
    Riem a)
    => [GenericTest a]
allRiemTests =
    genericTestGroup "Riem" riemTests : (allSmoothTests ++ allMetricTests)
