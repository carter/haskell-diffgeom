module DiffGeom.Classes.SmoothTest where

import Test.Framework
import Test.Framework.Providers.QuickCheck2
import Test.QuickCheck

import DiffGeom.Classes.TolSpace
import DiffGeom.Classes.TolSpaceTest
import DiffGeom.Classes.Smooth

-- |Enable generic testing
import Test.Generic

-- TODO: Put the fiber bundle and vector bundle tests here for TangentType and
-- CoTangentType

-- |Summary of tests
smoothTests :: (Arbitrary a, Show a, Smooth a) => [GenericTest a]
smoothTests = [ ]

allSmoothTests :: (Arbitrary a, Show a, Smooth a)
    => [GenericTest a]
allSmoothTests =
    genericTestGroup "Smooth" smoothTests : allTolSpaceTests
