module DiffGeom.Classes.TolSpaceTest where

import Test.Framework
import Test.Framework.Providers.QuickCheck2
import Test.QuickCheck

import DiffGeom.Classes.TolSpace

-- |Enable generic testing
import Test.Generic

-- |Test the axioms for a tolerance space
propTolReflexiveZero :: (TolSpace a) => a -> Property
propTolReflexiveZero x = property $ x ~~ x

propTolSymmetric :: (TolSpace a) => (a,a) -> Property
propTolSymmetric (x,y) = property $ (x ~~ y) == (y ~~ x)

-- |Summary of tests
tolSpaceTests :: (Arbitrary a, Show a, TolSpace a) => [GenericTest a]
tolSpaceTests =
 [ genericTestProperty "x ~~ x always" propTolReflexiveZero
 , genericTestPropertyPair "~~ is symmetric" propTolSymmetric
 ]

allTolSpaceTests :: (Arbitrary a, Show a, TolSpace a) => [GenericTest a]
allTolSpaceTests = [genericTestGroup "TolSpace" tolSpaceTests]
