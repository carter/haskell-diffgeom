module DiffGeom.Classes.VectorSpaceTest where

import Test.Framework
import Test.Framework.Providers.QuickCheck2
import Test.QuickCheck

import DiffGeom.Classes.TolSpace
import DiffGeom.Classes.TolSpaceTest
import DiffGeom.Classes.VectorSpace

-- |Enable generic testing
import Test.Generic

{- |Test the axioms of a 'RealVectorSpace'.
Note that we require a 'TolSpace' instance so that we can test equality reliably
-}
propPlusCommutes :: (RealVectorSpace a, TolSpace a) => (a, a) -> Property
propPlusCommutes (x, y) = property $ x .+. y ~~ y .+. x
propPlusAssoc :: (RealVectorSpace a, TolSpace a) => (a, a, a) -> Property
propPlusAssoc (x, y, z) = property $ x .+. (y .+. z) ~~ (x .+. y) .+. z
propZeroLeftId :: (RealVectorSpace a, TolSpace a) => a -> Property
propZeroLeftId x = property $ vzero .+. x ~~ x
propZeroRightId :: (RealVectorSpace a, TolSpace a) => a -> Property
propZeroRightId x = property $ x .+. vzero ~~ x
propAdditiveInv :: (RealVectorSpace a, TolSpace a) => a -> Property
propAdditiveInv x = property $ x .-. x ~~ vzero
propSubAdd :: (RealVectorSpace a, TolSpace a) => a -> Property
propSubAdd x = property $ x .+. ((-1) *. x) ~~ vzero
propMulId:: (RealVectorSpace a, TolSpace a) => a -> Property
propMulId x = property $ x .* 1 ~~ x
propScalarDiv :: (RealVectorSpace a, TolSpace a) => (a, Double) -> Property
propScalarDiv (x, s) = abs s > 1e-5 ==> x ./ s ~~ x .* (1/s)
propScalarMulCommutes :: (RealVectorSpace a, TolSpace a) => (a, Double) -> Property
propScalarMulCommutes (x, s) = property $ x .* s ~~ s *. x
propScalarMulAssoc :: (RealVectorSpace a, TolSpace a, Show a) => (a, Double, Double) -> Property
propScalarMulAssoc (x, r, s) = printTestCase tc $ property $ lass ~~ rass
  where
    lass = r *. (s *. x)
    rass = (r*s) *. x
    tc = "r*.(s*.x)=" ++show lass++" (r*s)*.x="++show rass

propScalarDistrib :: (RealVectorSpace a, TolSpace a, Show a) => (a, Double, Double) -> Property
propScalarDistrib (x, r, s) = printTestCase tc $ property $ ssum ~~ vsum
  where
    ssum = (r + s) *. x
    vsum = r *. x .+. s *. x
    tc = "(r+s)*.x=" ++ show ssum ++ " r*.x.+.s*.x=" ++ show vsum
propVectorDistrib :: (RealVectorSpace a, TolSpace a) => (a, a, Double) -> Property
propVectorDistrib (x, y, r) = property $ r *. (x .+. y) ~~ r *. x .+. r *. y

-- |Summary of 'VectorSpace' tests
realVectorSpaceTests :: (Arbitrary a, Show a, RealVectorSpace a, TolSpace a)
    => [GenericTest a]
realVectorSpaceTests =
    [ genericTestPropertyPair "Addition is commutative" propPlusCommutes
    , genericTestPropertyTriple "Addition is associative" propPlusAssoc
    , genericTestProperty "vbvzero is additive left identity" propZeroLeftId
    , genericTestProperty "vbvzero is additive right identity" propZeroRightId
    , genericTestProperty "Subtraction by self is zero" propAdditiveInv
    , genericTestProperty "Additive inverse is addition of neg" propSubAdd
    , genericTestProperty "Multiplicative identity" propMulId
    , genericTestPropertyDouble "Scalar division compatible with mult" propScalarDiv
    , genericTestPropertyDouble "Scalar multiplication commutes" propScalarMulCommutes
    , genericTestPropertyDoublePair "Scalar multiplication is associative" propScalarMulAssoc
    , genericTestPropertyDoublePair "Scalar distributivity" propScalarDistrib
    , genericTestPropertyPairDouble "Vector distributivity" propVectorDistrib
    ]

allRealVectorSpaceTests :: (Arbitrary a, Show a, RealVectorSpace a, TolSpace a)
    => [GenericTest a]
allRealVectorSpaceTests = 
    genericTestGroup "RealVectorSpace" realVectorSpaceTests : allTolSpaceTests

-- |'RealNormedSpace' tests

-- |Tolerance for floating point comparisons in these tests
tol = 1e-7

-- |floating point compare
(~?~) :: Double -> Double -> Bool
x ~?~ y = abs (x - y) <= (nSavg + 1) * tol
  where
    nSavg = 0.5 * (abs x + abs y)
infix 4 ~?~

propNonNeg :: (RealNormedSpace a, TolSpace a) => a -> Property
propNonNeg x = property $ norm x >= 0
propHomog :: (RealNormedSpace a, TolSpace a) => (a, Double) -> Property
propHomog (x, r) = property $ norm (r *. x) ~?~ (abs r) * norm x
propTriangleIneq :: (RealNormedSpace a, TolSpace a, Show a) => (a, a) -> Property
propTriangleIneq (x, y) = property $ norm (x .+. y) <= (norm x) + (norm y)
propNormSquared :: (RealNormedSpace a, TolSpace a, Show a) => a -> Property
propNormSquared x = printTestCase tc $ property $ normSquared x ~?~ (norm x)^2
  where
    tc = "normSquared x=" ++ show (normSquared x) ++ " (norm x)^2="++ show ((norm x)^2)

-- |Summary of 'RealNormedSpace' tests
realNormedSpaceTests :: (Arbitrary a, Show a, RealNormedSpace a, TolSpace a)
    => [GenericTest a]
realNormedSpaceTests = 
    [ genericTestProperty "Norm is non-negative" propNonNeg
    , genericTestPropertyDouble "Norm is 1-homogeneous" propHomog
    , genericTestPropertyPair "Triangle Inequality" propTriangleIneq
    , genericTestProperty "normSquared compatible with norm" propNormSquared
    ]

allRealNormedSpaceTests :: (Arbitrary a, Show a, RealNormedSpace a, TolSpace a)
    => [GenericTest a]
allRealNormedSpaceTests = 
    genericTestGroup "RealNormedSpace" realNormedSpaceTests : allRealVectorSpaceTests

-- |'RealInnerProductSpace' tests

propIPSym :: (RealInnerProductSpace a, TolSpace a) => (a, a) -> Property
propIPSym (x, y) = property $ innerProd x y ~?~ innerProd y x
propIPLinear :: (RealInnerProductSpace a, TolSpace a) => (a, a, Double) -> Property
propIPLinear (x, y, r) = property $ innerProd (r *. x) y ~?~ r * (innerProd y x)
propIPLinearVec :: (RealInnerProductSpace a, TolSpace a) => (a, a, a) -> Property
propIPLinearVec (x, y, z) = property $ innerProd (x .+. y) z ~?~ (innerProd x z) + (innerProd y z)
propIPNorm :: (RealInnerProductSpace a, TolSpace a) => a -> Property
propIPNorm x = property $ innerProd x x ~?~ normSquared x

-- |Summary of 'RealInnerProductSpace' tests
realInnerProductSpaceTests :: (Arbitrary a, Show a, TolSpace a,
    RealInnerProductSpace a) => [GenericTest a]
realInnerProductSpaceTests = 
    [ genericTestPropertyPair "Inner product is symmetric" propIPSym
    , genericTestPropertyPairDouble "Inner product is linear" propIPLinear
    , genericTestPropertyTriple "Inner product is addition linear" propIPLinearVec
    , genericTestProperty "Inner product compatible with norm" propIPNorm
    ]

allRealInnerProductSpaceTests :: (Arbitrary a, Show a, RealInnerProductSpace a, TolSpace a)
    => [GenericTest a]
allRealInnerProductSpaceTests = 
    genericTestGroup "RealInnerProductSpace" realInnerProductSpaceTests : allRealNormedSpaceTests

-- |'DualPair' tests

-- TODO: DualPair tests using Arbitrary pairs

{- |Summary of 'DualPair' tests.
Note that since 'DualPair' takes two parameters, we use a pair as the phantom
type.
-}
dualPairTests :: (Arbitrary a, Show a, TolSpace a,
                  Arbitrary b, Show b, TolSpace b,
                  DualPair a b)
    => [GenericTest (a, b)]
dualPairTests =
    [
    ]
