{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleContexts #-}

module Test.Generic where

import Test.Framework
import Test.Framework.Providers.QuickCheck2
import Test.QuickCheck

import DiffGeom.Classes.Smooth
import DiffGeom.Classes.Fiber

-- |Now use a phantom type parameter to force testing on various spaces
data GenericTest a = GenericTest Test
mkTest :: GenericTest a -> Test
mkTest (GenericTest x) = x

-- |Take a group of tests and perform them together, but keep phantom type
genericTestGroup :: TestName -> [GenericTest a] -> GenericTest a
genericTestGroup x ts = GenericTest $ testGroup x $ map mkTest ts

-- |Test a quickcheck property generically using :9
genericTestProperty :: (Arbitrary a, Show a) => TestName -> (a -> Property) -> GenericTest a
genericTestProperty x p = GenericTest $ testProperty x p

genericTestPropertyPair :: (Arbitrary a, Show a) => TestName -> ((a,a) -> Property) -> GenericTest a
genericTestPropertyPair x p = GenericTest $ testProperty x p

genericTestPropertyTriple :: (Arbitrary a, Show a) => TestName -> ((a,a,a) -> Property) -> GenericTest a
genericTestPropertyTriple x p = GenericTest $ testProperty x p

-- |Insert a TangentType into a test
genericTestPropertyTangent :: (Arbitrary (TangentType a), Show (TangentType a)) => TestName -> (TangentType a -> Property) -> GenericTest a
genericTestPropertyTangent x p = GenericTest $ testProperty x p
-- |Insert a CoTangentType into a test
genericTestPropertyCoTangent :: (Arbitrary (CoTangentType a), Show (CoTangentType a)) => TestName -> (CoTangentType a -> Property) -> GenericTest a
genericTestPropertyCoTangent x p = GenericTest $ testProperty x p
-- |Insert a BaseType from some FiberBundle into a test
genericTestPropertyBaseType :: (Arbitrary (BaseType a), Show (BaseType a)) => TestName -> (BaseType a -> Property) -> GenericTest a
genericTestPropertyBaseType x p = GenericTest $ testProperty x p

-- |Also throw in an arbitrary Double
genericTestPropertyDouble :: (Arbitrary a, Show a) => TestName -> ((a, Double) -> Property) -> GenericTest a
genericTestPropertyDouble x p = GenericTest $ testProperty x p
-- |Throw in a pair of doubles
genericTestPropertyDoublePair :: (Arbitrary a, Show a) => TestName -> ((a, Double, Double) -> Property) -> GenericTest a
genericTestPropertyDoublePair x p = GenericTest $ testProperty x p
-- |Need two a's and a Double
genericTestPropertyPairDouble :: (Arbitrary a, Show a) => TestName -> ((a, a, Double) -> Property) -> GenericTest a
genericTestPropertyPairDouble x p = GenericTest $ testProperty x p
