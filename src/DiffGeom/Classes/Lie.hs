{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}

module DiffGeom.Classes.Lie where

import DiffGeom.Classes.TolSpace
import DiffGeom.Classes.VectorSpace
{-import DiffGeom.Classes.Fiber-}
{-import DiffGeom.Classes.Metric-}
import DiffGeom.Classes.Smooth
import DiffGeom.Classes.Riem

{-import Control.Failure-}
import Data.Monoid

{- |This module provides Lie groups and Lie algebras, with typeclasses
representing groups with left, right, and bi-invariant Riemannian metrics.
-}

{- |First introduce a class for groups, which are just Monoids with inverses.
Instances should satisfy:

> forall x. x <> (invert x) ~~ mempty 
> forall x. (invert x) <> x ~~ mempty 
-}
class (TolSpace gp, Monoid gp) => Group gp where
    invert :: gp -> gp

{- |A type representing an abstract Lie algebra.
Instances should satisfy the bilinearity laws for a general associative
algebra:

> forall a,b,x,y. ad (a *. x .+. b *. y) z = a *. (ad x z) .+. b *. (ad y z)

the alternating property of ad:

> forall x,y. ad x y = vnegate (ad y x)

as well as the Jacobi identity:

> forall x,y,z. ad x (ad y z) .+. ad z (ad x y) .+. ad y (ad z x) = vzero
-}
class (RealVectorSpace v) => LieAlgebra v where
    ad :: v -> v -> v

{- |Duality takes a special form for Lie algebras, due to the adjoint
representation having a dual counterpart.
Instances should satisfy:

> forall m,w,v. pairing m (ad w v) = pairing (coad w m) v -- duality
-}
class (DualPair a b, LieAlgebra a) => DualLiePair a b where
    coad :: a -> b -> b

{- |Lie groups are groups, as well as Riemannian manifolds. They have associated Lie algebras and a representation over those algebras called the adjoint representation.  The following typeclass attempts to encode all of this.

Instances should ensure that left and right translation are compatibile
with the group operation and that they represent the derivative of the
group binary operation <>.  As this involves derivatives it is hard to
encode in haskell.

TODO: left and right translation away from identity and accompanying laws

Additionally, bigAd and bigCoAd should be dual operators:

> forall m,g,x. pairing m (bigAd g x) = pairing (bigCoAd g m) x -- duality

and bigAd should be the derivative of conjugation in the group.
-}
class (Group gp, Riem gp, DualLiePair (LieAlgType gp) (LieCoAlgType gp))
    => LieGroup gp where
    type LieAlgType gp
    type LieCoAlgType gp
    -- |Translation of a Lie algebra element to a tangent vector in the group
    leftTransId :: gp -> LieAlgType gp -> TangentType gp
    rightTransId :: LieAlgType gp -> gp -> TangentType gp
    -- |Adjoint and coadjoint representation
    bigAd :: gp -> LieAlgType gp -> LieAlgType gp
    bigCoAd :: gp -> LieCoAlgType gp -> LieAlgType gp
