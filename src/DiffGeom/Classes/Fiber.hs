{-# LANGUAGE TypeFamilies,FlexibleContexts,MultiParamTypeClasses #-}

module DiffGeom.Classes.Fiber (
    FiberBundle(..),
    VectorBundle(..),
    VectorBundleError(..),
    bundleSum,
    DualPairBundles(..)
) where

import DiffGeom.Classes.TolSpace

import Control.Failure
import Control.Monad (foldM)

{- |Just a simple 'FiberBundle' class supporting projection to a base
manifold

This class has no laws, except that projBase should be continuous and
surjective, and 'BaseType' should be a 'Smooth' manifold.
-}
class (TolSpace (BaseType e)) => FiberBundle e where
    type BaseType e
    -- |Basepoint projection.  Should be continuous, surjective.
    projBase :: e -> BaseType e

{- |A 'VectorBundle' is a 'FiberBundle' for which, restricted to a single
point, each fiber is a vectorspace.

To encode this in Haskell, we provide the usual vector space methods, but
require that if points in the base manifold are not the same, we return
Nothing.  So we have the usual real vector space laws:

> a ^+^ b = b ^+^ a (additive commutativity)
> a ^+^ (b ^+^ c) = (a ^+^ b) ^+^ c (additive associativity)
> vbzero ^+^ a = a ^+^ vbzero = a (additive identity)
> a ^+^ (-1 *^ a) = 0 (additive inverse is negation)
> a *^ v = v ^* a (scalar-vector multiplication commutativity)
> a *^ (b *^ v) = (a*b) *^ v (multiplicative associativity)
> (a + b) *^ v = (a *^ v) + (b *^ v) (distributivity of scalars)
> a *^ (v + w) = (a *^ v) + (a *^ w) (distributivity of vectors)
> 1 *^ v = v (multiplicative identity)

Along with the following laws concerning mismatched base points:

> forall v,w. projBase v ~~ projBase w => v ^+^ w = Just x
> else v ^+^ w = Nothing

TODO: Fixity for these operators matching that of those in 'Num'
-}
class (FiberBundle e) => VectorBundle e where
    -- |The usual vector space operations
    (*^) :: Double -> e -> e
    (^*) :: e -> Double -> e
    infixl 7 *^
    infixl 7 ^*
    (^+^) :: (Failure VectorBundleError m) => e -> e -> m e
    (^-^) :: (Failure VectorBundleError m) => e -> e -> m e
    infixl 6 ^+^
    infixl 6 ^-^
    -- |Note that a base point must be given in order to get a zero vector
    vbzero :: BaseType e -> e
    vbnegate :: e -> e
    -- |Default implementations
    (*^) = flip (^*)
    (^*) = flip (*^)  -- This is a law, usually no reason to override this
    x ^-^ y = x ^+^ ((-1) *^ y)  -- Also not usually a reason to override
    vbnegate x = (-1) *^ x

-- |Error type for vector bundle related errors
data VectorBundleError = VBArithmetic | VBSumEmpty | VBDualBasePt
instance Show VectorBundleError where
    show VBArithmetic = "Vector bundle arithmetic requires matching base points"
    show VBSumEmpty = "Attempted bundleSum of empty list"
    show VBDualBasePt = "Dual pairing requires matching base point"

-- |With this we can define sum of an arbitrary set of vectorbundle points
bundleSum :: (VectorBundle e, Failure VectorBundleError m) => [e] -> m e
bundleSum [] = failure VBSumEmpty
bundleSum l@(v:_) = foldM (^+^) (vbzero $ projBase v) l

{- |The 'VectorBundleWithDual' type encodes a pair of a vector bundle with
its dual.  Since every vector space has a dual, building DualType as an
associated type to 'VectorBundle' leads to a cycle that the type checker
will complain about.

Note that in order to use these in complicated expressions, it's more
convenient to work inside the Maybe monad and use do notation

Also note that every 'VectorBundle' instance must also declare its dual
type, which entails also providing the method 'dualPairing'.  The
'BaseType's of these must match:

> (VectorBundle e) => BaseType (DualType e) ~ BaseType e

Instances should obey the following laws related to basepoint matching
inside 'dualPairing':

> forall v,w. projBase v ~~ projBase w => dualPairing v w = Just x
> else dualPairing v w = Nothing
> dualPairing (a ^+^ b) w = fmap $ a `dualPairing` w + b `dualPairing` w
> dualPairing v (a ^+^ b) = fmap $ v `dualPairing` a + v `dualPairing` b
> dualPairing (a ^-^ b) w = fmap $ a `dualPairing` w - b `dualPairing` w
> dualPairing v (a ^-^ b) = fmap $ v `dualPairing` a - v `dualPairing` b
> dualPairing (a *^ v) w = fmap $ a * (v `dualPairing` w)
> dualPairing (v ^* a) w = fmap $ a * (v `dualPairing` w)
> dualPairing v (a *^ w) = fmap $ a * (v `dualPairing` w)
> dualPairing v (w ^* a) = fmap $ a * (v `dualPairing` w)
-}
class (VectorBundle d, VectorBundle p, BaseType d ~ BaseType p)
      => DualPairBundles p d where
    -- |Contract a vector into a dual vector to produce a scalar
    bundlePairing :: (Failure VectorBundleError m) => d -> p -> m Double
