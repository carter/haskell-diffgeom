{-# LANGUAGE FlexibleContexts #-}

module DiffGeom.Classes.Riem (
    Riem(..),
    RiemError(..),
    riemNorm,
    riemNormSquared,
    riemNormalize
)
where

{-import DiffGeom.Classes.TolSpace-}
import DiffGeom.Classes.Fiber
import DiffGeom.Classes.Metric
import DiffGeom.Classes.Smooth

import Data.Maybe
import Control.Failure

{- | A manifold with a Riemannian metric
Instances should satisfy the following laws:

> forall q,p. expMap (logMap q p) = p
> sharp . flat = id
> flat . sharp = id
> riemMetric w v = bundlePairing (flat w) v
> riemCoMetric w v = bundlePairing w (sharp v)

The Riemannian metric needs to also be compatible with the 'Metric' 'dist'
function in the following sense:

> riemMetric (logMap p q) (logMap p q) = (dist p q) * (dist p q)

TODO: How to rigorously write a law relating sharp/flat, riemMetric, and
expMap/logMap
-}
class (Smooth m, Metric m) => Riem m where
    -- |Raise indices: apply cometric tensor
    sharp :: CoTangentType m -> TangentType m
    -- |Lower indices: apply metric tensor
    flat :: TangentType m -> CoTangentType m

    -- |Compute the inner product of two tangent vectors if p ~~ q
    riemMetric :: (Failure VectorBundleError e) => TangentType m -> TangentType m -> e Double

    -- |Inverse metric used to inner product covectors
    riemCoMetric :: (Failure VectorBundleError e) => CoTangentType m -> CoTangentType m -> e Double

    -- |Riemannian exponential map
    expMap :: TangentType m -> m
    -- |Riemannian log map
    logMapUnsafe :: m -> m -> TangentType m

    -- |In general, log map is partial or non-unique
    logMap :: (Failure RiemError e) => m -> m -> e (TangentType m)

    -- |Default methods
    riemMetric w = bundlePairing (flat w)
    riemCoMetric w v = bundlePairing w (sharp v)

data RiemError = RiemLogCutLocus | RiemNormalize
instance Show RiemError where
    show RiemLogCutLocus = "Riemannian log map attempted for point outside cut locus"
    show RiemNormalize = "Cannot normalize zero tangent vector"

riemNormSquared :: Riem r => TangentType r -> Double
riemNormSquared v = fromMaybe 0 $ riemMetric v v
-- ^we know the above riemMetric call is safe

riemNorm :: Riem r => TangentType r -> Double
riemNorm = sqrt . riemNormSquared

riemNormalize :: Riem r => TangentType r -> TangentType r
riemNormalize v
    | l == 0 = v
    | otherwise = l *^ v
  where
    l = 1 / riemNorm v
