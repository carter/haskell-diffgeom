{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module DiffGeom.Classes.Actions where

{-import DiffGeom.Classes.TolSpace-}
{-import DiffGeom.Classes.Fiber-}
{-import DiffGeom.Classes.Metric-}
import DiffGeom.Classes.Smooth
import DiffGeom.Classes.Riem
import DiffGeom.Classes.Lie

import Data.Monoid

{-import Control.Failure-}

{- |Classes and methods relating to Lie group actions on vector spaces and
Riemannian manifolds.

A left monoid action of Monoid m on a set s.
Instances of 'LeftAction' should satisfy the following laws:

> leftAct mempty = id (identity action)
> leftAct (g <> h) = (leftAct g) . (leftAct h)

Note that normally we'll consider group actions on 'Riem' manifolds, but we
have no reason to encode this in the class just yet.

Also note that, technically, we could define semigroup actions as well, but the
current mood in the Haskell community seems to be to skip semigroups and go
straight to 'Monoid's, and until we have a specific reason not to, I'll go with
that consensus.
-}
class (Monoid m) => LeftAction m s where
    leftAct :: m -> s -> s

{- |A left Lie group action also defines an action of the Lie algebra on
the group, as well as its dual mapping, a momentum map.
Instances should satisfy the following laws:

In addition, leftActInf should coincide with the differential of leftAct
-}
class (LieGroup gp, Riem r, LeftAction gp r) => LeftLieGroupAction gp r where
    leftActInf :: LieAlgType gp -> r -> TangentType r
    leftMomMap :: CoTangentType r -> LieCoAlgType gp

-- TODO: Right actions, just satisfy the swapped laws but have roughly same
-- interface
