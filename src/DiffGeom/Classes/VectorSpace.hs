{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UndecidableInstances #-}

module DiffGeom.Classes.VectorSpace where

import DiffGeom.Classes.TolSpace
{-import DiffGeom.Classes.Metric-}
import DiffGeom.Classes.Fiber
import DiffGeom.Classes.Smooth

import Control.Failure

{- |Reinventing the wheel, I know.  Ideally we'd just use the classes from
numeric-prelude.
Instances should satisfy the following laws:

> forall x,y. x .+. y = y .+. x -- additive commutativity
> forall x,y,z. x .+. (y .+. z) = (x .+. y) .+. z -- additive associativity
> forall x. vzero .+. x = x -- additive identity
> forall x. x .-. x = vzero -- additive inverse
> forall x,y. x .-. y = x .+. (-1 *. y) -- subtraction is addition
> forall x. 1 *. x = x -- multiplicative identity
> forall a,x. (x .* a) ./ a = x -- multiplicative scalar inverse
> forall r,x. r *. x = x .* r -- scalar multiplication commutes
> forall r,s,x. r *. (s *. x) = (r * s) *. x -- multiplicative associativity 
> forall r,s,x. (r + s) *. x = r *.x + s *.x -- scalar distributivity
> forall r,x,y. r *. (x .+. y) = r *.x + r *.y -- vector distributivity
-}
class RealVectorSpace v where
    (.+.) :: v -> v -> v
    (.-.) :: v -> v -> v
    infixl 6 .+.
    infixl 6 .-.
    (*.) :: Double -> v -> v
    (.*) :: v -> Double -> v
    (./) :: v -> Double -> v
    infixl 7 *.
    infixl 7 .*
    infixl 7 ./
    vzero :: v
    vnegate :: v -> v
    -- |Default methods
    (.*) = flip (*.)
    (*.) = flip (.*)
    x ./ a = x .* (1/a)
    x .-. y = x .+. vnegate y
    vnegate = ((-1) *.)

{- |We also encode the notion of a dual vector space, which is a space
associated to every real vector space, and consists of all linear
functionals on v.  Note that the dual space is also a vector space.
Instances should satisfy the following laws:

> forall m,x,y. m `pairing` (x .+. y) = m `pairing` x + m `pairing` y -- linearity in v
> forall m,n,x. (m .+. n) `pairing` x = m `pairing` x + n `pairing` x -- linearity in d
> forall a,m,x. m `pairing` (a *. x) = a * (m `pairing` x) -- linearity
> forall a,m,x. (a *. m) `pairing` x = a * (m `pairing` x) -- linearity
> forall m. m `pairing` vzero = 0 -- zero
> forall x. vzero `pairing` x = 0 -- zero
-}
class (RealVectorSpace d, RealVectorSpace v) => DualPair d v where
    pairing :: d -> v -> Double

{- |A normed vector space also includes a 1-homogeneous norm function
satisfying the triangle inequality.
Instances should satisfy the following laws:

> forall x. norm x > 0 -- non-negativity
> forall a,x. norm (a *. x) = (abs a) * (norm x) -- 1-homogeneity
> forall x,y. norm (x .+. y) <= (norm x) + (norm y) -- triangle inequality

In addition, the norm and normSquared functions should be compatible:

> forall x. normSquared x = (norm x) * (norm x)

Minimal definition: one of `norm` or `normSquared`
-}
class (RealVectorSpace a) => RealNormedSpace a where
    norm :: a -> Double
    normSquared :: a -> Double
    -- |Default methods.
    norm = sqrt . normSquared
    normSquared x = let n = norm x in n * n

data VSError = VSNormalize
instance Show VSError where
    show VSNormalize = "Zero vector cannot be normalized"

-- |Normalize a vector by dividing by norm
normalize :: (RealNormedSpace a, Failure VSError m) => a -> m a
normalize v = if isInfinite b || isNaN b then failure VSNormalize else return (v .* b)
  where
    b = 1 / norm v

-- |An unsafe version of the above that does not check for zero norm
normalizeUnsafe :: RealNormedSpace a => a -> a
normalizeUnsafe v = v ./ norm v

{- |A 'RealNormedSpace' can be given a 'TolSpace' instance using the
following helper function, by choosing an acceptable relative tolerance.-}
vecTolCompare :: (RealNormedSpace a) => Double -> a -> a -> Bool
vecTolCompare tol p q = normSquared (p .-. q) <= tol * (nSavg + 1)
      where
        nSavg = 0.5 * (normSquared p + normSquared q)

{- |An inner product space is a vector space in which an inner (dot)
product between vectors exists.
Instances should satisfy the following laws:

> forall x,y. innerProd x y = innerProd y x -- symmetry
> forall a,x,y. innerProd (a *. x) y = a * (innerProd x y) -- linearity
> forall x,y,z. innerProd (x .+. y) z = (innerProd x z) + (innerProd y z) -- linearity
> forall x. innerProd x x = normSquared x -- compatibility
-}
class (RealNormedSpace a) => RealInnerProductSpace a where
    innerProd :: a -> a -> Double

{- |Finally, it will be useful to treat vector spaces just like abstract
Riemannian manifolds at time.  For that purpose, we provide the following
instances.
-}
newtype (RealVectorSpace v, TolSpace v) => VSManifold v = VSManifold {unMfd :: v}
instance (RealVectorSpace v, TolSpace v) => TolSpace (VSManifold v) where
    x ~~ y = unMfd x ~~ unMfd y -- un-manifold then compare

-- type synonym for tangent type that's just a simple pair
type VSTangent v = (VSManifold v, v)

instance (RealVectorSpace v, TolSpace v) => TolSpace (VSTangent v) where
    (p, v) ~~ (q, w) = p ~~ q && v ~~ w

instance (RealVectorSpace v, TolSpace v) => FiberBundle (VSTangent v) where
    type BaseType (VSTangent v) = VSManifold v
    projBase ((x, _)) = x

instance (RealVectorSpace v, TolSpace v) => VectorBundle (VSTangent v) where
    a *^ (p, v) = (p, a *. v)
    (p, v) ^* a = (p, v .* a)
    (p, v) ^+^ (q, w)
        | p ~~ q = return (p, v .+. w)
        | otherwise = failure VBArithmetic
    (p, v) ^-^ (q, w)
        | p ~~ q = return (p, v .-. w)
        | otherwise = failure VBArithmetic
    vbzero p = (p, vzero)

{- |Assuming you've given an instance of DualPair to your vector space,
meaning you've defined a bilinear "dual pairing" of your vector space over
itself, then there is an associated dual pairing of vector bundles.
-}
instance (TolSpace v, DualPair v v) => DualPairBundles (VSTangent v) (VSTangent v) where
    bundlePairing (p,m) (q,x)
        | p ~~ q = return $ pairing m x
        | otherwise = failure VBDualBasePt

{- |Once you've defined such a dual pairing, not only do you get a dual
bundle, but using VSManifold, you get a Smooth manifold
-}
instance (RealVectorSpace v, TolSpace v, DualPair v v) => Smooth (VSManifold v) where
    type TangentType (VSManifold v) = VSTangent v
    type CoTangentType (VSManifold v) = VSTangent v

{- |If your vector space is equipped with and inner product, you can also
get a Riemannian structure
-}
