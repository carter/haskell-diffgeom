module DiffGeom.Classes.Metric (
    Metric(..)
) where

import DiffGeom.Classes.TolSpace

{- |The 'Metric' typeclass encodes the notion of a metric space.
Instances should satisfy the following metric space laws:

> dist p q = dist q p
> dist p q >= 0
> dist p q = 0  iff  p ~~ q (see below)
> dist a c <= dist a b + dist b c (triangle inequality)

The equality law is meant to only hold approximately in the case that m is
not also an instance of Eq.

-}
class (TolSpace m) => Metric m where
    dist :: m -> m -> Double
    distSquared :: m -> m -> Double
    -- |Default methods. Only one is required
    dist x y = sqrt $ distSquared x y
    distSquared x y = let d = dist x y in d * d
