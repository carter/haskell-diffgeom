{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleContexts #-}

module DiffGeom.Classes.Smooth (
    Smooth(..)
) where

import DiffGeom.Classes.TolSpace
import DiffGeom.Classes.Fiber

{- |The 'Smooth class represents points and tangent vectors on a
smooth manifold

This class has no methods currently, but ideally we'd be able to do things
like take the differential of an arbitrary function, define vector field
types, etc.
-}
class ( TolSpace m,
        TolSpace (TangentType m),
        TolSpace (CoTangentType m),
        DualPairBundles (TangentType m) (CoTangentType m),
        BaseType (TangentType m) ~ m
        ) => Smooth m where
    type TangentType m
    type CoTangentType m
