{-# LANGUAGE FlexibleInstances #-}

module DiffGeom.Classes.TolSpace  where

{- |The 'TolSpace' class is for spaces which have points that can be
compared.  It's basically the Eq class but where we are be able to do
"fuzzy" comparison of points, which is often useful in practice.

Instances should satisfy the following laws:

> p ~~ p = True  (Reflexivity)
> p ~~ q = q ~~ p (Symmetry)

This means that 'TolSpace' represents a "tolerance relation".  As such, a
common instance is some metric space in which points "close enough"
together return True under ~~.

The method ~~ is nearly an equivalence relation like ==, but is "fuzzy" in
the sense that we relax the Transitivity constraint.
-}
class TolSpace s where
    (~~) :: s -> s -> Bool
    infix 4 ~~
