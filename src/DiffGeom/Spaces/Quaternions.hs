{-# LANGUAGE MultiParamTypeClasses #-}

module DiffGeom.Spaces.Quaternions where

import DiffGeom.Classes.TolSpace
import DiffGeom.Classes.VectorSpace
{-import DiffGeom.Classes.Lie-}
import DiffGeom.Classes.Actions

import DiffGeom.Spaces.Vec3

import Data.Monoid

{- |This module provides types for quaternions and unit quaternions, and
their associated Lie group instances.
-}

-- |We now define a quaternion in terms of scalars and 3-vectors.
data Quatern = Quatern Double Vec3 deriving Show

-- |Simple utility to grab the vector part of a quaternion
quatVecPart :: Quatern -> Vec3
quatVecPart (Quatern _ v) = v

{- |The quaternion conjugate (analogous to complex conjugate).  Simply
negate the vector part
-}
quatConj :: Quatern -> Quatern
quatConj (Quatern a v) = Quatern a (vnegate v)

-- |Binary multiplication operation for quaternions
quatMult :: Quatern -> Quatern -> Quatern
quatMult (Quatern a va) (Quatern b vb) = Quatern p vp
  where
    p = a * b - dot va vb
    vp = (a *. vb) .+. (b *. va) .+. cross va vb

-- |Vector space instance uses standard R^4 structure on quaternions
instance RealVectorSpace Quatern where
    (Quatern a v) .+. (Quatern b w) = Quatern (a + b) (v .+. w)
    (Quatern a v) .-. (Quatern b w) = Quatern (a - b) (v .-. w)
    (Quatern a v) .* b = Quatern (a * b) (v .* b)
    vzero = Quatern 0 vzero
    vnegate (Quatern a v) = Quatern (-a) (vnegate v)

-- |Quaternions have a standard norm, inherited from R^4
instance RealNormedSpace Quatern where
    normSquared (Quatern a v) = (a*a) + dot v v

-- |..and an inner product as well
instance RealInnerProductSpace Quatern where
    innerProd (Quatern a v) (Quatern b w) = (a*b) + dot v w

{- |Let's make it a TolSpace by introducing a floating point comparison
TODO: select tolerance for these floating point comparisons in some
principled way
-}
instance TolSpace Quatern where
    (~~) = vecTolCompare 1e-10

-- |Now a Monoid instance using quaternion multiplication and identity
instance Monoid Quatern where
    mempty = Quatern 1 vzero
    mappend = quatMult

{- |Quaternions act on 3-vectors by conjugation
Note that this implementation is slightly inefficient as it involves some
terms that are multiplied by a constant 0.  I'm not sure those get
optimized away.
-}
instance LeftAction Quatern Vec3 where
    leftAct q p = quatVecPart $ q <> Quatern 0 p <> quatConj q
