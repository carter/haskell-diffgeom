{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module DiffGeom.Spaces.Sphere where

import DiffGeom.Classes.TolSpace
import DiffGeom.Classes.VectorSpace
import DiffGeom.Classes.Fiber
import DiffGeom.Classes.Smooth
import DiffGeom.Classes.Metric
import DiffGeom.Classes.Riem

import DiffGeom.Spaces.Vec3
{-import DiffGeom.Spaces.Quaternions-}

import Control.Failure

{-import Control.Exception.Base-}

{- |A very simple 2-sphere implementation.

We use 3-vectors to represent points on the sphere, as opposed to
latitude-longitude, or other representations.  This means that certain
operations are faster, but that we must always be vigilant and make sure
our sphere points keep length=1.
-}
newtype SpherePt = SpherePt Vec3 deriving (Show)

-- |Simple way to project a Vec3 to the sphere
toSphere :: Vec3 -> Maybe SpherePt
toSphere = fmap SpherePt . normalize

-- |Unsafe version that can produce NaNs if you pass in the zero vector
toSphereUnsafe :: Vec3 -> SpherePt
toSphereUnsafe = SpherePt . normalizeUnsafe

-- |We inherit our tolerance instance
instance TolSpace SpherePt where
    (SpherePt x) ~~ (SpherePt y) = vecTolCompare 1e-8 x y

{- |Before describing the manifold structure, we need to introduce a
tangent bundle type for the sphere.  This is basically just a pair of a
sphere point along with a 'Vec3', except that we will require the two to be
always orthogonal.
-}
data SphereTangent = SphereTangent (SpherePt, Vec3)

-- |Trivial fiber bundle and vector bundle instances
instance FiberBundle SphereTangent where
    type BaseType SphereTangent = SpherePt
    projBase (SphereTangent (p, _)) = p
instance VectorBundle SphereTangent where
    a *^ (SphereTangent (p, v)) = SphereTangent (p, a *. v)
    (SphereTangent (p, v)) ^+^ (SphereTangent (q, w))
        | p ~~ q = return $ SphereTangent (p, v .+. w)
        | otherwise = failure VBArithmetic
    (SphereTangent (p, v)) ^-^ (SphereTangent (q, w))
        | p ~~ q = return $ SphereTangent (p, v .-. w)
        | otherwise = failure VBArithmetic
    vbzero p = SphereTangent (p, vzero)
instance TolSpace SphereTangent where
    SphereTangent (p, v) ~~ SphereTangent (q, w) = p ~~ q && c v w
      where
        c = vecTolCompare 1e-8
instance Show SphereTangent where
    show pv = "SphereTangent " ++ show pv

-- |Project a Vec3 to be tangent at p
projSphereTangent :: SpherePt -> Vec3 -> SphereTangent
projSphereTangent pp@(SpherePt p) v = SphereTangent (pp, vproj)
  where
    vproj = v .-. p .* dot v p

{- |Now we introduce a Cotangent Type which looks just the same as
SphereTangent.

TODO: I have to figure out how to reduce the boilerplate.  Really I just
need to decorate a single type with *Tangent, and *CoTangent and give the
above instances to each.  I'm not sure how to do that at this point so
there's a lot of duplicate code.
-}
data SphereCoTangent = SphereCoTangent (SpherePt, Vec3)

instance FiberBundle SphereCoTangent where
    type BaseType SphereCoTangent = SpherePt
    projBase (SphereCoTangent (p, _)) = p
instance VectorBundle SphereCoTangent where
    a *^ (SphereCoTangent (p, v)) = SphereCoTangent (p, a *. v)
    (SphereCoTangent (p, v)) ^+^ (SphereCoTangent (q, w))
        | p ~~ q = return $ SphereCoTangent (p, v .+. w)
        | otherwise = failure VBArithmetic
    (SphereCoTangent (p, v)) ^-^ (SphereCoTangent (q, w))
        | p ~~ q = return $ SphereCoTangent (p, v .-. w)
        | otherwise = failure VBArithmetic
    vbzero p = SphereCoTangent (p, vzero)
instance TolSpace SphereCoTangent where
    SphereCoTangent (p, v) ~~ SphereCoTangent (q, w) = p ~~ q && v ~~ w
instance Show SphereCoTangent where
    show pm = "SphereCoTangent " ++ show pm

{- |Now that we split out both of those chunks, we can give a simple
'DualPairing' instance.
-}
instance DualPairBundles SphereTangent SphereCoTangent where
    bundlePairing (SphereCoTangent (p, m)) (SphereTangent (q, v))
        | p ~~ q = return $ dot m v
        | otherwise = failure VBDualBasePt

-- |Now give the simple Smooth instance
instance Smooth SpherePt where
    type TangentType SpherePt = SphereTangent
    type CoTangentType SpherePt = SphereCoTangent

-- |Sphere distances are easy to compute, using dot product
instance Metric SpherePt where
    dist (SpherePt x) (SpherePt y) = acos $ clamp $ dot x y
      where
        clamp z
            | z > 1 = 1
            | z < -1 = -1
            | otherwise = z

{- |Finally, we give the standard Riemannian structure to the sphere.
-}
instance Riem SpherePt where
    -- |Sharp/flat is the identity in this representation
    sharp (SphereCoTangent pm) = SphereTangent pm
    flat (SphereTangent pv) = SphereCoTangent pv

    -- |exp, log using standard formulas
    expMap (SphereTangent (SpherePt p, v)) = case vh of
        Nothing -> SpherePt p
        Just vhv -> toSphereUnsafe $ (p .* c) .+. (vhv .* s)
      where
        nv = norm v
        vh = normalize v
        c = cos nv
        s = sin nv

    logMap ps@(SpherePt p) qs@(SpherePt q)
        | q ~~ vnegate p = failure RiemLogCutLocus
        | otherwise = return $ logMapUnsafe ps qs

    logMapUnsafe pp@(SpherePt p) qq@(SpherePt q)
        | pp ~~ qq = vbzero pp
        | otherwise = SphereTangent (pp, vl)
      where
        vl = maybe vzero (d *.) $ normalize v
        c = dot p q
        d = dist pp qq
        v = q .-. c *. p  -- project orthogonal
