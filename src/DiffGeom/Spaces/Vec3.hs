module DiffGeom.Spaces.Vec3 where

import DiffGeom.Classes.TolSpace
import DiffGeom.Classes.VectorSpace

-- |We start with a simple fixed-length 3-vector type.
data Vec3 = Vec3 Double Double Double deriving (Show,Eq,Read)

-- |We will give an instance of RealVectorSpace to Vec3 as well
instance RealVectorSpace Vec3 where
    (Vec3 a b c) .+. (Vec3 x y z) = Vec3 (a+x) (b+y) (c+z)
    (Vec3 a b c) .-. (Vec3 x y z) = Vec3 (a-x) (b-y) (c-z)
    a *. (Vec3 x y z) = Vec3 (a*x) (a*y) (a*z)
    vzero = Vec3 0 0 0
    vnegate (Vec3 x y z) = Vec3 (-x) (-y) (-z)

-- |Trivial instances using standard Euclidean geometry
instance RealNormedSpace Vec3 where
    normSquared (Vec3 x y z) = x*x + y*y + z*z
instance RealInnerProductSpace Vec3 where
    innerProd = dot

-- |Now set a somewhat arbitrary tolerance and give a TolSpace instance
instance TolSpace Vec3 where
    (~~) = vecTolCompare 1e-15

{- |Dot product defined for 3-vectors.  This can serve as an inner product
or dual pairing.
-}
dot :: Vec3 -> Vec3 -> Double
dot (Vec3 a b c) (Vec3 x y z) = a*x + b*y + c*z

-- |Cross product. This can serve as ad in a LieAlgebra instance.
cross :: Vec3 -> Vec3 -> Vec3
cross (Vec3 a b c) (Vec3 x y z) = Vec3 ci cj ck
  where
    ci = b*z - c*y
    cj = c*x - a*z
    ck = a*y - b*x
