{-# LANGUAGE MultiParamTypeClasses #-}

module DiffGeom.Spaces.Rotations where

import DiffGeom.Classes.TolSpace
import DiffGeom.Classes.Lie
import DiffGeom.Classes.VectorSpace
import DiffGeom.Classes.Actions

import DiffGeom.Spaces.Vec3
import DiffGeom.Spaces.Quaternions

import Data.Monoid

{- |This module provides a representation of 3D rotations, modeled by the
Lie group SO(3).  Internally, we use the unit quaternion representation.
The associated Lie algebra is the space so(3) of skew-symmetric matrices,
represented by 3-vectors with bracket given by the vector cross-product.
-}
newtype Rot3 = Rot3 Quatern deriving (Show)

{- |Most instances for Rot3 are simply inherited from Quatern.  However, we
take special care to renormalize whenever doing an operation which might
cause the norm of our quaternion to stray from 1.0.

The (re)normalization operation is represented by the 'fromQuatern'
function:
-}
fromQuatern :: Quatern -> Maybe Rot3
fromQuatern q = fmap Rot3 $ normalize q

-- |Now we just start inheriting instances in a pretty rote manner.
instance TolSpace Rot3 where
    (Rot3 q) ~~ (Rot3 p) = q ~~ p
instance Monoid Rot3 where
    mempty = Rot3 mempty
    (Rot3 q) `mappend` (Rot3 p) = Rot3 $ q `mappend` p
instance Group Rot3 where
    invert (Rot3 q) = Rot3 $ quatConj q ./ normSquared q
{- |Rot3 acts on R^3, but not transitively.  We will use this group action
to write a controlled action on the unit sphere later.
-}
instance LeftAction Rot3 Vec3 where
    leftAct (Rot3 q) p = q `leftAct` p

{- |In order to provide a LieGroup instance, we need to define a type for
the Lie algebra, so(3).  This is the space of skew-symmetric matrices with
matrix commutator, but we can represent it efficiently using Vec3 and the
cross product:
-}
newtype So3base = So3base Vec3 deriving (Show, Read, Eq)

-- |More boring manual derivation of vector space structure
instance RealVectorSpace So3base where
    (So3base v) .+. (So3base w) = So3base (v .+. w)
    (So3base v) .-. (So3base w) = So3base (v .-. w)
    (So3base v) .* b = So3base (v .* b)
    vzero = So3base vzero
    vnegate (So3base v) = So3base (vnegate v)
