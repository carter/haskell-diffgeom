module DiffGeom.Algorithms.Optim where

import DiffGeom.Classes.Fiber
import DiffGeom.Classes.Smooth
import DiffGeom.Classes.Riem

{- |This module implements very general gradient-based optimization schemes
on Riemannian manifolds.  The user is referred to the excellent papers by
Wolfgang Ring, Benedict Wirth and others.
-}

{- |The fundamental object is the notion of a differentiable scalar
function defined over a Riemannian manifold.

The type DiffableFn r attempts to formalize this notion.  Given such an
object, one may evaluate it using 'evalFn', or take its (Riemannian)
gradient using 'riemGrad'.

TODO: I don't currently know how to enforce the following "law" for this
type:

> forall p. p == projBase $ riemGrad p

Nor do I know how to enforce other properties of the gradient, such as for
all p there exists a stepsize for which a step along the gradient increases
the function.

TODO: This might be a lens, and maybe a profunctor, or instance of Category
somehow
-}
newtype (Smooth r) => DiffableFn r = DiffableFn (r -> (Double, CoTangentType r))
evalFn :: (Smooth r) => DiffableFn r -> r -> Double
evalFn (DiffableFn f) = fst . f
grad :: (Smooth r) => DiffableFn r -> r -> CoTangentType r
grad (DiffableFn f) = snd . f

-- |On a Riemannian manifold, we can also take the Riemannian gradient
riemGrad :: (Riem r) => DiffableFn r -> r -> TangentType r
riemGrad f = sharp . grad f

-- |A simple gradient step. Report current energy, and next step)
gradStep :: (Riem r) => DiffableFn r -> Double -> r -> (Double, r)
gradStep f s p = (evalFn f pnext, pnext)
  where
    pnext = expMap $ negate s *^ riemGrad f p

-- |A simple gradient step, but ignore the energy
gradStep' :: (Riem r) => DiffableFn r -> Double -> r -> r
gradStep' f s p = snd $ gradStep f s p

{- |Fixed stepsize gradient descent uses a stream of points, to which we
can add stopping criteria or do other fancy stuff.
-}
fixedGDSteps :: (Riem r) => DiffableFn r -> Double -> r -> [(Double, r)]
fixedGDSteps f s p = tail $ iterate go (0, p)
  where
    go (_, p') = gradStep f s p'

-- |Simple fixed stepSize gradient descent with no stopping criterion
fixedGD :: (Riem r) => DiffableFn r -> Double -> Int -> r -> (Double, r)
fixedGD f s n p = fixedGDSteps f s p !! n

