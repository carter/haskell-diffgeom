module DiffGeom.Algorithms.Frechet where

import DiffGeom.Classes.Riem
{-import DiffGeom.Classes.Smooth-}
import DiffGeom.Classes.Fiber

import DiffGeom.Algorithms.Optim

{-import Control.Applicative ((<$>))-}

import Data.Maybe (fromMaybe)
{-import Data.List (foldl')-}

{-import Control.Failure-}

{- |The Frechet mean is based on optimization of the Frechet variance,
which we declare as a differentiable function
-}
frechetVar :: (Riem r) => [r] -> DiffableFn r
frechetVar [] = DiffableFn (\p -> (0, vbzero p))
frechetVar xs = DiffableFn f
  where
    f x = (sseLogs x, flat $ vbnegate $ sumLogs x)
    logMaps x = map (logMapUnsafe x) xs
    sseLogs x = sum $ map riemNormSquared $ logMaps x
    -- Note that we know we will never get Nothing out of bundleSum, as
    -- long as the laws for Riem are obeyed
    sumLogs x = fromMaybe (vbzero x) $ bundleSum $ logMaps x

{- |Simple synonym for fixed stepsize gradient descent with no stopping
condition
-}
frechetMeanFixed :: (Riem r) => [r] -> Double -> Int -> r -> (Double, r)
frechetMeanFixed = fixedGD . frechetVar

-- |The Frechet median minimizes the sum of non-squared distances to the
-- data. The algorithm is nearly identical to that of the Frechet mean.
-- However, instead of using the sum of logs, one uses the sum of
-- "normalized" logs, where the normalization refers to division by norm
-- cf. Fletcher, Venkatasubramanian, and Joshi 2008
frechetL1 :: (Riem r) => [r] -> DiffableFn r
frechetL1 [] = DiffableFn (\p -> (0, vbzero p))
frechetL1 xs = DiffableFn f
  where
    f x = (sseLogs x, flat $ sumLogs x)
    logMaps x = map (logMapUnsafe x) xs
    sseLogs x = sum $ map riemNorm $ logMaps x
    -- Note that we know we will never get Nothing out of bundleSum, as
    -- long as the laws for Riem are obeyed
    sumLogs x = fromMaybe (vbzero x) $ bundleSum $ logMaps x

frechetMedianFixed :: (Riem r) => [r] -> Double -> Int -> r -> (Double, r)
frechetMedianFixed = fixedGD . frechetL1
